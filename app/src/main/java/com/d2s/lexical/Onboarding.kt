package com.d2s.lexical

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import com.d2s.lexical.databinding.OnboardingBinding
import com.d2s.lexical.ui.onboarding.OnboardingAdapter
import com.d2s.lexical.ui.onboarding.OnboardingItem

class Onboarding : AppCompatActivity(){

    private lateinit var binding: OnboardingBinding
    private lateinit var activity: Activity
    private lateinit var onboardingAdapter: OnboardingAdapter
    private lateinit var layoutOnboardingIndicator: LinearLayout
    private lateinit var btnNext: Button
    private lateinit var preference: SharedPreferences
    private val prefShowIntro = "Intro"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = OnboardingBinding.inflate(layoutInflater)
        layoutOnboardingIndicator = binding.onboardingIndicatorsLayout
        btnNext = binding.onboardingActionButton
        setContentView(binding.root)

        setupOnboardingItems()
        val onboardingViewPager: ViewPager2 = binding.onboardingViewPager
        onboardingViewPager.adapter = onboardingAdapter
        setupOnboardingIndicators()
        setCurrentOnboardingIndicator(0, btnNext)
        activity = this
        preference = getSharedPreferences("IntroSlider", Context.MODE_PRIVATE)

        //if onboarding is disabled in preferences
        if (!preference.getBoolean(prefShowIntro, true)) {
            startActivity(Intent(activity, Search::class.java))
            finish()
        }

        onboardingViewPager.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setCurrentOnboardingIndicator(position, btnNext)
            }
        })

        btnNext.setOnClickListener {
            if (onboardingViewPager.currentItem + 1 < onboardingAdapter.itemCount) {
                onboardingViewPager.currentItem = onboardingViewPager.currentItem + 1
            } else {
                goToSearchActivity()
            }
        }
    }

    private fun setupOnboardingItems() {
        val onboardingItems: MutableList<OnboardingItem> = ArrayList()

        // First slide
        val itemWelcome = OnboardingItem()
        itemWelcome.image = R.drawable.logo_d2s
        itemWelcome.title = "Welcome"
        itemWelcome.description =
            "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas porttitor congue massa. "

        // Second slide
        val item2 = OnboardingItem()
        item2.image = R.drawable.logo
        item2.title = "Fusce"
        item2.description =
            "Fusce posuere, magna sed pulvinar ultricies, purus lectus malesuada libero, sit amet commodo magna eros quis urna."

        // Third slide
        val item3 = OnboardingItem()
        item3.image = R.drawable.icons8_search
        item3.title = "Vivamus"
        item3.description =
            "Vivamus a tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Proin pharetra nonummy pede. Mauris et orci. Aenean nec lorem."

        onboardingItems.add(itemWelcome)
        onboardingItems.add(item2)
        onboardingItems.add(item3)

        onboardingAdapter = OnboardingAdapter()
    }

    private fun setupOnboardingIndicators() {
        val indicators: MutableList<ImageView> = ArrayList()
        val layoutParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutParams.setMargins(8, 0, 8, 0)

        indicators.forEachIndexed { _, indicator ->
            indicator.setImageDrawable(
                ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.onboarding_indicator_inactive
                )
            )
            indicator.layoutParams = layoutParams
            layoutOnboardingIndicator.addView(indicator)
        }
    }

    private fun setCurrentOnboardingIndicator(index: Int, buttonNext: Button) {
        val childCount: Int = layoutOnboardingIndicator.childCount

        for (i in 1..childCount) {
            val imageView: ImageView = layoutOnboardingIndicator.getChildAt(i) as ImageView
            if (i == index) {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.onboarding_indicator_active
                    )
                )
            } else {
                imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.onboarding_indicator_inactive
                    )
                )
            }
        }

        if (index == onboardingAdapter.itemCount - 1) {
            //The last page
            buttonNext.text = getString(R.string.done)
        } else {
            //Has Next
            buttonNext.text = getString(R.string.next)
        }
    }

    private fun goToSearchActivity() {
        startActivity(Intent(activity, Search::class.java))
        finish()
        val editor = preference.edit()
        editor.putBoolean(prefShowIntro, false)
        editor.apply()
    }

}
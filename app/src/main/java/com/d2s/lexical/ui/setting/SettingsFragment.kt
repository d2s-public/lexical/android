package com.d2s.lexical.ui.setting

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import com.d2s.lexical.R

class SettingsFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }

}
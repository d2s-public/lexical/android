package com.d2s.lexical.ui.entries

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.d2s.lexical.databinding.FragmentDialogEntryBinding
import com.d2s.lexical.models.Entry
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    EntryDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 */
class EntryDialogFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentDialogEntryBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var entry: Entry

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentDialogEntryBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.entryHeadWord.text = entry.headword
        binding.entryPartOfSpeech.text = entry.partOfSpeech
        binding.entryMeaning.text = entry.meaning
        binding.entryExample.text = entry.exemple
        binding.entryVariant.text = entry.variant?.headword ?: ""
        //binding.entrySynonyms.text = entry.synonyms
        //todo: Linkify synonyms array
    }

    companion object {

        fun newInstance(entry: Entry): EntryDialogFragment =
            EntryDialogFragment().apply {
                this.entry = entry
            }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
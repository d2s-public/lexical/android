package com.d2s.lexical.ui.topics

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.d2s.lexical.databinding.FragmentEntriesListBinding

/**
 * A fragment representing a list of Items.
 */
class TopicsFragment : Fragment() {

    private var _binding: FragmentEntriesListBinding? = null
    private lateinit var viewModel: TopicsViewModel

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentEntriesListBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this)[TopicsViewModel::class.java]
        load()

        return binding.root
    }

    fun load() {
        viewModel.topicsList.observe(viewLifecycleOwner) { topics ->

            binding.list.layoutManager = GridLayoutManager(context, 2)
            binding.list.adapter = TopicsRecyclerViewAdapter(requireContext(), topics)
        }
    }
}
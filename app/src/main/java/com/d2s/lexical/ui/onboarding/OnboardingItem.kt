package com.d2s.lexical.ui.onboarding

class OnboardingItem {
    var image: Int = 0
    var title: String = ""
    var description: String = ""
}
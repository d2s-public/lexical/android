package com.d2s.lexical.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.d2s.lexical.Main
import com.d2s.lexical.databinding.RecyclerItemPopularTopicsBinding
import com.d2s.lexical.models.Topic
import com.d2s.lexical.ui.entries.EntriesListFragment

class PopularTopicsAdapter(private val context: Context, private val topics: List<Topic>) :
    RecyclerView.Adapter<PopularTopicsAdapter.TopicsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicsViewHolder {
        return TopicsViewHolder(
            RecyclerItemPopularTopicsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TopicsViewHolder, position: Int) {
        holder.fill(topics[position])
    }

    override fun getItemCount(): Int {
        return topics.size
    }

    inner class TopicsViewHolder(binding: RecyclerItemPopularTopicsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var topicWord: TextView = binding.topicWord

        fun fill(topic: Topic) {
            topicWord.text = topic.name
            topicWord.setOnClickListener {
                //goto entries list, filtered by topic.name
                (context as Main).changeFragment(EntriesListFragment())
                Toast.makeText(context, "Topic: ${topic.name} wanted", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

}

package com.d2s.lexical.ui.topics

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.d2s.lexical.Main
import com.d2s.lexical.databinding.FragmentTopicsItemBinding
import com.d2s.lexical.models.Topic
import com.d2s.lexical.ui.entries.EntriesListFragment
import com.d2s.lexical.ui.entries.EntriesViewModel

/**
 * [RecyclerView.Adapter] that can display a [Topic].
 * TODO: Replace the implementation with code for your data type.
 */
class TopicsRecyclerViewAdapter(
    private val context: Context,
    private val values: List<Topic>
) : RecyclerView.Adapter<TopicsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentTopicsItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fill(values[position])
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentTopicsItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val card: CardView = binding.topicCard
        val name: TextView = binding.topicName
        val checkBox: CheckBox = binding.topicIsChecked

        fun fill(topic: Topic) {
            name.text = topic.name
            checkBox.isChecked = topic.checked

            checkBox.setOnClickListener {}
            card.setOnClickListener {
                //filter entries list and open EntriesListFragment
                val entriesViewModel =
                    ViewModelProvider((context as FragmentActivity))[EntriesViewModel::class.java]
                entriesViewModel.filterByTopic(topic)
                (context as Main).changeFragment(EntriesListFragment())
            }
        }
    }

}
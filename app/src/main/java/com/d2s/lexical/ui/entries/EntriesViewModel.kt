package com.d2s.lexical.ui.entries

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.d2s.lexical.api.ApiClient
import com.d2s.lexical.api.ApiInterface
import com.d2s.lexical.models.Entry
import com.d2s.lexical.models.Topic
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EntriesViewModel : ViewModel() {

    private var apiInterface: ApiInterface =
        ApiClient.getRetrofitInstance().create(ApiInterface::class.java)

    private val _entriesList = MutableLiveData<List<Entry>>().apply {
        val call: Call<List<Entry>> = apiInterface.getAllEntries()
        call.enqueue(object : Callback<List<Entry>> {
            /**
             * Invoked for a received HTTP response.
             *
             *
             * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
             * Call [Response.isSuccessful] to determine if the response indicates success.
             */
            override fun onResponse(call: Call<List<Entry>>, response: Response<List<Entry>>) {
                value = response.body()
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<List<Entry>>, t: Throwable) {
                Log.e(
                    "Categories call",
                    "MSG->${t.localizedMessage} \n " +
                            "CAUSE->${t.cause} \n"
                )
            }

        })
    }
    val entriesList: LiveData<List<Entry>> = _entriesList

    fun filterByTopic(topic: Topic) {
        _entriesList.value?.filter {
            it.topic == topic
        }
    }

}
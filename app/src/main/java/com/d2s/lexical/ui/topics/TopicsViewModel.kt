package com.d2s.lexical.ui.topics

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.d2s.lexical.api.ApiClient
import com.d2s.lexical.api.ApiInterface
import com.d2s.lexical.models.Topic
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TopicsViewModel : ViewModel() {

    private var apiInterface: ApiInterface =
        ApiClient.getRetrofitInstance().create(ApiInterface::class.java)

    private val _topicsList = MutableLiveData<List<Topic>>().apply {
        val call: Call<List<Topic>> = apiInterface.getAllTopics()
        call.enqueue(object : Callback<List<Topic>> {
            /**
             * Invoked for a received HTTP response.
             *
             *
             * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
             * Call [Response.isSuccessful] to determine if the response indicates success.
             */
            override fun onResponse(call: Call<List<Topic>>, response: Response<List<Topic>>) {
                value = response.body()
                //Log.i("API RESPONSE", response.body().toString())
                //Log.i("VALUE", value.toString())
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected exception
             * occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<List<Topic>>, t: Throwable) {
                Log.e(
                    "Categories call",
                    "MSG->${t.localizedMessage} \n " +
                            "CAUSE->${t.cause} \n"
                )
            }

        })
    }
    val topicsList: LiveData<List<Topic>> = _topicsList

}
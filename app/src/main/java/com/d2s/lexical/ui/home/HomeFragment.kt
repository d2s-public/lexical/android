package com.d2s.lexical.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.d2s.lexical.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private lateinit var homeViewModel: HomeViewModel

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        load()
        homeViewModel.entriesList.observe(viewLifecycleOwner) {
            it.filter { entry ->
                entry.headword.contains(homeViewModel.queryString.value?.trim()!!.lowercase(), true)
            }
        }
        //TODO: filter list with queryString from main activity
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun load() {
        try {
            homeViewModel.topicsList.observe(viewLifecycleOwner) {
                //binding.popularTopicsRecyclerView.layoutManager =
                //    LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                binding.popularTopicsRecyclerView.adapter =
                    PopularTopicsAdapter(requireContext(), it!!)
            }
        } catch (e: Exception) {
            Log.e("Exception", e.message!!)
        }
    }
}
package com.d2s.lexical.ui.onboarding

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.d2s.lexical.R
import java.util.*

class OnboardingAdapter : Adapter<OnboardingAdapter.OnboardingViewHolder>() {

    private var onboardingItems: List<OnboardingItem> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OnboardingViewHolder {

        return OnboardingViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_container_onboarding, parent, false)
        )
    }
    override fun onBindViewHolder(@NonNull holder: OnboardingViewHolder, position: Int) {
        holder.setOnboardingData(onboardingItems[position])
    }

    override fun getItemCount(): Int {
        return onboardingItems.size
    }

    inner class OnboardingViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var title: TextView = itemView.findViewById(R.id.onboardingTitle)
        private var description: TextView = itemView.findViewById(R.id.onboardingDescription)
        private var image: ImageView = itemView.findViewById(R.id.onboardingImage)


        fun setOnboardingData(onboardingItem: OnboardingItem) {
            image.setImageResource(onboardingItem.image)
            title.text = onboardingItem.title
            description.text = onboardingItem.description
        }
    }

}
package com.d2s.lexical.ui.entries

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.d2s.lexical.databinding.FragmentEntriesListItemBinding
import com.d2s.lexical.models.Entry

/**
 * [RecyclerView.Adapter] that can display a [Entry].
 * TODO: Replace the implementation with code for your data type.
 */
class EntriesRecyclerViewAdapter(
    private val context: Context,
    private val values: List<Entry>
) : RecyclerView.Adapter<EntriesRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            FragmentEntriesListItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.loadValues(values[position])
        holder.entry.setOnClickListener {
            EntryDialogFragment.newInstance(
                values[position]
            ).show((context as FragmentActivity).supportFragmentManager, "dialog")
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentEntriesListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val entry: ConstraintLayout = binding.entryItem
        val id: TextView = binding.entryItemId
        private val headWord: TextView = binding.entryItemHeadword
        private val meaning: TextView = binding.entryItemMeaning

        fun loadValues(entry: Entry) {
            id.text = entry.id.toString()
            headWord.text = entry.headword
            meaning.text = entry.meaning
        }
    }

}
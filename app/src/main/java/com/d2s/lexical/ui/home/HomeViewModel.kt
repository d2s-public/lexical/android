package com.d2s.lexical.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.d2s.lexical.api.ApiClient
import com.d2s.lexical.api.ApiInterface
import com.d2s.lexical.models.Entry
import com.d2s.lexical.models.Topic
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {

    private var apiInterface: ApiInterface =
        ApiClient.getRetrofitInstance().create(ApiInterface::class.java)

    private val _queryString = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val queryString: LiveData<String> = _queryString

    fun setQueryString(newString: String?) {
        _queryString.value = newString
    }

    private val _topicsList = MutableLiveData<List<Topic>>().apply {
        val call: Call<List<Topic>> = apiInterface.getAllTopics()
        call.enqueue(object : Callback<List<Topic>> {
            override fun onResponse(call: Call<List<Topic>>, response: Response<List<Topic>>) {
                if (response.body().isNullOrEmpty()) {
                    Log.i("API RESPONSE", response.body().toString())
                } else {
                    value = response.body()
                }
                //Log.i("VALUE", value.toString())
            }

            override fun onFailure(call: Call<List<Topic>>, t: Throwable) {
                Log.e(
                    "Categories call",
                    "MSG->${t.localizedMessage} \n " +
                            "CAUSE->${t.cause} \n"
                )
            }

        })
    }
    val topicsList: LiveData<List<Topic>> = _topicsList

    private val _entriesList = MutableLiveData<List<Entry>>().apply {
        val call: Call<List<Entry>> = apiInterface.getAllEntries()
        call.enqueue(object : Callback<List<Entry>> {
            override fun onResponse(call: Call<List<Entry>>, response: Response<List<Entry>>) {
                value = response.body()
            }

            override fun onFailure(call: Call<List<Entry>>, t: Throwable) {
                Log.e(
                    "Categories call",
                    "MSG->${t.localizedMessage} \n " +
                            "CAUSE->${t.cause} \n"
                )
            }

        })
    }
    val entriesList: LiveData<List<Entry>> = _entriesList

}
package com.d2s.lexical.ui.entries

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.d2s.lexical.databinding.FragmentEntriesListBinding

/**
 * A fragment representing a list of Items.
 */
class EntriesListFragment : Fragment() {

    private var _binding: FragmentEntriesListBinding? = null
    private lateinit var viewModel: EntriesViewModel

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEntriesListBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this)[EntriesViewModel::class.java]
        load()

        return binding.root
    }

    private fun load(): Boolean {
        viewModel.entriesList.observe(viewLifecycleOwner) { entries ->
            binding.list.layoutManager = LinearLayoutManager(context)
            binding.list.adapter = EntriesRecyclerViewAdapter(requireContext(), entries)
        }

        return true
    }
}
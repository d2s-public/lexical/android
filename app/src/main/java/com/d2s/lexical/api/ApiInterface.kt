package com.d2s.lexical.api

import com.d2s.lexical.models.Entry
import com.d2s.lexical.models.Topic
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    //@Headers("accept: application/json")

    @GET("entry")
    fun getAllEntries(): Call<List<Entry>>

    @GET("entry/{entry}")
    fun getEntry(@Path("entry") entry: Entry): Call<Entry>

    @GET("topic")
    fun getAllTopics(): Call<List<Topic>>

}

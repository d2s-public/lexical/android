package com.d2s.lexical.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {
    companion object {
        fun getRetrofitInstance(): Retrofit {
            var retrofit: Retrofit? = null
            val BASE_URL = "http://localhost:8000/api/"

            if (retrofit == null)
                retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build()
            return retrofit!!
        }
    }
}
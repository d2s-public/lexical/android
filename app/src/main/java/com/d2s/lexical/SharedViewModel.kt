package com.d2s.lexical

import androidx.lifecycle.ViewModel

/**
 * Use to share data between search activity and between fragments within Main
 */
class SharedViewModel: ViewModel() {
}
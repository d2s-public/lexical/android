package com.d2s.lexical

import android.animation.ObjectAnimator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.d2s.lexical.databinding.SplashBinding

class Splash : AppCompatActivity() {
    private lateinit var binding: SplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SplashBinding.inflate(layoutInflater)

        val splashProgressBar = binding.splashProgressBar

        setContentView(binding.root)

        Handler(Looper.getMainLooper()).postDelayed({
            ObjectAnimator.ofInt(splashProgressBar, "progress", 600)
                .setDuration(2500)
                .start()
            startActivity(Intent(applicationContext, Search::class.java))
            finish()
        }, 3000)
    }
}
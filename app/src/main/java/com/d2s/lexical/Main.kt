package com.d2s.lexical

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.d2s.lexical.ui.home.HomeViewModel
import com.google.android.material.navigation.NavigationView

class Main : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_entry_list,
                R.id.nav_topics,

                R.id.nav_splash
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        try {
            homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
            val bundle: Bundle? = intent.extras
            val queryString = bundle!!.getString("queryString", "")
            homeViewModel.setQueryString(queryString)
            //            toolbar.title = queryString
        } catch (e: Exception) {
            //Log.d("VM", "Exception with view model", e)
        }

        loadSettings()
    }

    private fun loadSettings() {
        val sharedPreferences: SharedPreferences =
            getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        //val theme: String? = sharedPreferences.getString("APPEARANCE.THEME", null)
        //Toast.makeText(this, "Preferences ${sharedPreferences.toString()}", Toast.LENGTH_LONG)
        //    .show()
    }

    private fun saveSettings() {
        val sharedPreferences: SharedPreferences =
            getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.apply {
            //appearance
            putString("APPEARANCE.THEME", "theme_name")
            //behaviour
            putBoolean("BEHAVIOUR.SESSION", true)
            putBoolean("BEHAVIOUR.SPLASH", false)
            //network
            putBoolean("NETWORK.SHARE_AUTO", false)
        }.apply()
    }

    fun changeFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.nav_host_fragment, fragment)
            //.addToBackStack(null)
            .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return NavigationUI.onNavDestinationSelected(
            item,
            findNavController(R.id.nav_host_fragment)
        ) || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

}
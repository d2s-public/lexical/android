package com.d2s.lexical.models

data class Topic(
    var id: Int,
    var name: String,
    var slug: String?,
    val parent: Topic?,
    val entries: MutableList<Entry>? = ArrayList(),

    var checked: Boolean = false
)

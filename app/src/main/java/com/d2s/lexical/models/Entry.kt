package com.d2s.lexical.models

data class Entry(
    var id: Int?,
    var headword: String,
    var partOfSpeech: String?,
    var meaning: String?,
    var exemple: String?,
    var variant: Entry?,
    var synonyms: List<Entry>?,
    var topic: Topic? = null,
    var author: User?
)

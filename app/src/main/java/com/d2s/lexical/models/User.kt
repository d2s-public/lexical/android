package com.d2s.lexical.models

data class User(
    var id:Int,
    var image:String,
    var name:String
) {

}

package com.d2s.lexical

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import com.d2s.lexical.databinding.SearchBinding
import com.d2s.lexical.ui.home.HomeViewModel

class Search : AppCompatActivity() {
    private lateinit var homeFragmentViewModel: HomeViewModel

    private lateinit var binding: SearchBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        homeFragmentViewModel = ViewModelProvider(this)[HomeViewModel::class.java]

        val linkToMain = binding.linkToMain
        linkToMain.setOnClickListener {
            //            Toast.makeText(this@Search, "Textview clicked", Toast.LENGTH_LONG).show()
            gotoMain("")
        }

        val btnSearchGo = binding.btnSearchGo
        btnSearchGo.setOnClickListener {
            //            Toast.makeText(this@Search, "Button clicked", Toast.LENGTH_LONG).show()
            gotoMain(binding.searchView.query.toString())
        }
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            /**
             * Called when the user submits the query. This could be due to a key press on the
             * keyboard or due to pressing a submit button.
             * The listener can override the standard behavior by returning true
             * to indicate that it has handled the submit request. Otherwise return false to
             * let the SearchView handle the submission by launching any associated intent.
             *
             * @param query the query text that is to be submitted
             *
             * @return true if the query has been handled by the listener, false to let the
             * SearchView perform the default action.
             */
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            /**
             * Called when the query text is changed by the user.
             *
             * @param newText the new content of the query text field.
             *
             * @return false if the SearchView should perform the default action of showing any
             * suggestions if available, true if the action was handled by the listener.
             */
            override fun onQueryTextChange(newText: String?): Boolean {
                gotoMain(newText)
                return false
            }
        })
    }

    private fun gotoMain(queryString: String?) {
        val intent = Intent(applicationContext, Main::class.java)
        if (!queryString.isNullOrBlank())
            intent.putExtra("queryString", queryString)
        //homeFragmentViewModel.setQueryString(queryString)

        startActivity(intent)
        finish()
    }
}